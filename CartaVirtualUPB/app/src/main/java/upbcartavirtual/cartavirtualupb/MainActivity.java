package upbcartavirtual.cartavirtualupb;

import android.app.Activity;
import android.content.Intent;//importada
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;//importada

public class MainActivity extends Activity {

    private static final String whatsApp = "com.whatsapp";//Se declara privada y asigna al paquete de whatsapp

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ConsultarUbicacion(View v){//El view no se usa, es para decir que hace una referencia ala vista(boton)

        Intent intencion = new Intent(this , MapsActivity.class); // Intent es de android.content.Intent, los parametros son this que dice que esta actividad hara la intencion y que la hara a MapsActivity.class
        startActivity(intencion); //lanza la intencion que creamos
    }

    public void VerMenu(View v){//El view no se usa, es para decir que hace una referencia ala vista(boton)

        Intent intencion = new Intent(this , CartaActivity.class); // Intent es de android.content.Intent, los parametros son this que dice que esta actividad hara la intencion y que la hara a MapsActivity.class
        startActivity(intencion); //lanza la intencion que creamos
    }
    public void ReservarMesa(View v){//El view no se usa, es para decir que hace una referencia ala vista(boton)

        PackageManager pm = getPackageManager();//El SO va a llamar al administrador de paquetes
        Intent intent = pm.getLaunchIntentForPackage(whatsApp);//Se crea una intencion a la que se asigna la funcionalidad que se lanzara
        startActivity(intent);
    }


}
