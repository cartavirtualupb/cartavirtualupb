package upbcartavirtual.cartavirtualupb;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.app.Activity;


import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(6.2420118,-75.5913483), 17));

        // Marcador boulevar
        LatLng boulevar = new LatLng(6.2416119, -75.5896158);
        mMap.addMarker(new MarkerOptions().position(boulevar).title("Sede Boulevar"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(boulevar));

        //Marcador Arqui
        LatLng arqui = new LatLng(6.2403584, -75.5896913);
        mMap.addMarker(new MarkerOptions().position(arqui).title("Sede Arquidiseño"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(arqui));

        //Marcador Derecho
        LatLng derecho = new LatLng(6.2433424, -75.5905829);
        mMap.addMarker(new MarkerOptions().position(derecho).title("Sede Derecho"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(derecho));
    }
}
