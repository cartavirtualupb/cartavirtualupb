package upbcartavirtual.cartavirtualupb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class CartaActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);

    }

    public void verEntradas(View v){

        Intent intencion = new Intent(this,EntradaActivity.class);
        startActivity(intencion);

    }

    public void verFuertes(View v){

        Intent intencion = new Intent(this,PlatoActivity.class);
        startActivity(intencion);

    }

    public void verBebidas(View v){

        Intent intencion = new Intent(this,BebidaActivity.class);
        startActivity(intencion);

    }

    public void verPostres(View v){

        Intent intencion = new Intent(this,PostreActivity.class);
        startActivity(intencion);

    }
}
